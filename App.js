import React, {useRef, useState} from 'react'
import {SafeAreaView, StatusBar, StyleSheet, View, Image, Text} from 'react-native';
import {Button} from "react-native-paper";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {MainPage} from "./MainPage";
import {Delivery} from "./Delivery";
import {CardDelivery} from "./CardDelivery";

export default function App() {
    const Stack = createStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={MainPage} options={{headerShown: false}}/>
                <Stack.Screen name="Delivery" component={Delivery} options={{headerShown: false}}/>
                <Stack.Screen name="CardDelivery" component={CardDelivery} options={{headerShown: false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
