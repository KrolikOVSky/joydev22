import {Image, SafeAreaView, StatusBar, StyleSheet, Text, View} from "react-native";
import {Button} from "react-native-paper";
import React from "react";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tinyLogo: {
        width: '70%',
        aspectRatio: 1,
    },
    mainText: {
        fontWeight: '800',
        fontSize: 48,
    },
    button: {
        width: '60%',
    },
    btnContainer: {
        marginTop: 20
    },
    btnText: {
        fontWeight: 'bold',
        textTransform: 'uppercase',
    }
});

export const MainPage = ({navigation, ...props}) => {
    return <SafeAreaView style={{flex: 1}}>
        <StatusBar showHideTransition='fade'/>
        <View style={styles.container}>
            <Image
                style={styles.tinyLogo}
                source={{
                    uri: 'https://firebasestorage.googleapis.com/v0/b/unify-v3-copy.appspot.com/o/8a4z7qneilt-254%3A13?alt=media&token=87f0b297-ada6-40b1-b351-f7bffecd26b5',
                }}
            />
            <Text style={styles.mainText}>
                JoyDev22
            </Text>
            <Text style={styles.mainText}>
                Delivery
            </Text>
            <View style={styles.btnContainer}>
                <Button mode="contained"
                        onPress={() => navigation.navigate('Delivery')}
                        buttonColor={'#ff0'}
                        textColor={'#000'}
                        labelStyle={styles.button}
                >
                    <Text style={styles.btnText}>
                        Start Using
                    </Text>
                </Button>
            </View>
        </View>
    </SafeAreaView>
}