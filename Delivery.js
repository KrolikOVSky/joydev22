import {SafeAreaView, StatusBar, StyleSheet, Text, View} from "react-native";
import React from "react";
import {CourierView} from "./CourierView";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    header: {
        fontSize: 52,
        fontWeight: '100',
        marginTop: 20,
        marginLeft: 20
    },
    couriersContainer: {
        display: "flex",
        alignItems: 'center',
        width: '100%',
        height: '100%',
        marginTop: 50,
        gap: 30
    },
    courierContainer: {
        display: "flex",
        flexDirection: 'row',
        gap: 10,
        width: '60%',
        alignItems: "center"
    },
    icon: {
        borderRadius: 50,
        width: 50,
        height: 50
    },
    rating: {
        borderRadius: 50,
        width: 15,
        height: 15
    },
    courierInfo: {},
    courierName: {
        fontSize: 18
    },
    ratingContainer: {
        display: "flex",
        flexDirection: 'row',
        gap: 5,

    }

});

export const Delivery = ({navigation, ...props}) => {
    return <SafeAreaView style={{flex: 1}}>
        <StatusBar showHideTransition='fade'/>
        <View style={styles.container}>
            <Text style={styles.header}>
                Couriers
            </Text>
            <View style={styles.couriersContainer}>
                <CourierView name={'Alexander V.'}
                             rating={4.9}
                             navigation={navigation}
                />
                <CourierView name={'Dima S.'}
                             rating={4.9}
                             navigation={navigation}
                />
                <CourierView name={'Stepan M.'}
                             rating={4.9}
                             navigation={navigation}
                />
            </View>
        </View>
    </SafeAreaView>
}