const platform = new H.service.Platform({
    'apikey': 'WVhZG0KhkxTEuGxxEqLrDQ5II96O5zsNRJWdyufK_hE'
});

// Obtain the default map types from the platform object:
const defaultLayers = platform.createDefaultLayers({
    lg: 'ru'
});

// Instantiate (and display) a map:
let map = new H.Map(
    document.getElementById("map"),
    // Center the map on Great Britain with the zoom level of 6:
    defaultLayers.vector.normal.map, {
        zoom: 13.7,
        center: {
            lat: 44.597366,
            lng: 33.488055
        }
    });


//44.616817, 33.524625
// MapEvents enables the event system.
// The behavior variable implements default interactions for pan/zoom (also on mobile touch environments).
const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Enable dynamic resizing of the map, based on the current size of the enclosing cntainer
window.addEventListener('resize', () => map.getViewPort().resize());

// Create the default UI:
let ui = H.ui.UI.createDefault(map, defaultLayers);

//44.591273, 33.458211
const origin = {
    lat: 44.591273,
    lng: 33.458211
}
const origin2 = {
    lat: 44.591273,
    lng: 33.458211
}

//44.608435, 33.517167
const destination = {
    lat: 44.608435,
    lng: 33.517167
}
const destination2 = {
    lat: 44.608435,
    lng: 33.517167
}

const routingParams = {
    'routingMode': 'fast',
    'origin': `${origin.lat},${origin.lng}`,
    'destination': `${destination.lat},${destination.lng}`,
    'transportMode': 'car',
    'optimizeFor': 'quality',
    'return': 'polyline,turnByTurnActions,actions,instructions,travelSummary'
};

const routingParams2 = {
    'routingMode': 'fast',
    'origin': `${origin.lat},${origin.lng}`,
    'destination': `${destination.lat},${destination.lng}`,
    'transportMode': 'pedestrian',
    'optimizeFor': 'quality',
    'return': 'polyline,turnByTurnActions,actions,instructions,travelSummary'
};


function getMarkerIcon(id) {
    const svgCircle = `<svg width="30" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg">
                           <g id="marker">
                             <circle cx="15" cy="15" r="10" fill="#0099D8" stroke="#0099D8" stroke-width="4" />
                             <text x="50%" y="50%" text-anchor="middle" fill="#FFFFFF" font-family="Arial, sans-serif" font-size="12px" dy=".3em">${id}</text>
                           </g></svg>`;
    return new H.map.Icon(svgCircle, {
        anchor: {
            x: 10,
            y: 10
        }
    });
}

function addMarker(position, id) {
    const marker = new H.map.Marker(position, {
        data: {
            id
        },
        icon: getMarkerIcon(id),
    });

    map.addObject(marker);
    return marker;
}

addMarker(origin, 'A');
addMarker(destination, 'B');

const router = platform.getRoutingService(null, 8);

const defaultStyle = {
    lineWidth: 3,
}

const selectedStyle = {
    lineWidth: 5,
    strokeColor: "red",
}

function routeResponseHandler(response, lineWidth) {
    let routePolyline;
    const sections = response.routes[0].sections;
    console.log(sections)
    const lineStrings = [];
    sections.forEach((section) => {
        // convert Flexible Polyline encoded string to geometry
        lineStrings.push(H.geo.LineString.fromFlexiblePolyline(section.polyline));
    });
    const multiLineString = new H.geo.MultiLineString(lineStrings);
    const bounds = multiLineString.getBoundingBox();
    // Create the polyline for the route
    if (routePolyline) {
        // If the routePolyline we just set the new geometry
        routePolyline.setGeometry(multiLineString);
    } else {
        // routePolyline is not yet defined, instantiate a new H.map.Polyline
        routePolyline = new H.map.Polyline(multiLineString, {
            style: defaultStyle
        });
    }
    routePolyline.addEventListener('tap', (e) => {
        routePolyline.setStyle(selectedStyle);
        const routes = map.getObjects()
            .map(item => !item.icon && item)
            .filter(item => item !== false)
        routes.map(item => {
            console.log(item)
            if (item === routePolyline) {
                item.setStyle(selectedStyle);
            } else {
                item.setStyle(defaultStyle);
            }
        })
    })
    // Add the polyline to the map
    map.addObject(routePolyline);
}

function updateRoute() {
    router.calculateRoute(routingParams2, (e) => routeResponseHandler(e, 2), console.error);
    router.calculateRoute(routingParams, (e) => routeResponseHandler(e, 2), console.error);
}

updateRoute();

function onSuccess(result) {
    let route = result.routes;

    /*
     * The styling of the route response on the map is entirely under the developer's control.
     * A representative styling can be found the full JS + HTML code of this example
     * in the functions below:
     */
    // ... etc.
}