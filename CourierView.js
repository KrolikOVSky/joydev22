import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React from "react";

const styles = StyleSheet.create({
    courierContainer: {
        display: "flex",
        flexDirection: 'row',
        gap: 10,
        width: '60%',
        alignItems: "center"
    },
    icon: {
        borderRadius: 50,
        width: 50,
        height: 50
    },
    rating: {
        borderRadius: 50,
        width: 15,
        height: 15
    },
    courierInfo: {},
    courierName: {
        fontSize: 18
    },
    ratingContainer: {
        display: "flex",
        flexDirection: 'row',
        gap: 5,
    },
    socials: {
        display: "flex",
        flexDirection: "row",
        gap: 10
    }
});
export const CourierView = ({name, rating, socials = false, navigation, id}) => {
    return <TouchableOpacity style={styles.courierContainer}
                             onPress={() => navigation.navigate('CardDelivery', 'id2')}
    >
        <View>
            <Image style={styles.icon}
                   source={{
                       uri: 'https://firebasestorage.googleapis.com/v0/b/unify-v3-copy.appspot.com/o/s7grabfghh-216%3A43?alt=media&token=ac1066a1-ff03-4547-8944-65fa136d8a4f'
                   }}/>
        </View>
        <View>
            <Text style={styles.courierName}>
                {name}
            </Text>
            <View style={styles.ratingContainer}>
                <Image style={styles.rating}
                       source={{
                           uri: 'https://firebasestorage.googleapis.com/v0/b/unify-v3-copy.appspot.com/o/s7grabfghh-216%3A40?alt=media&token=bb04f045-70cd-445f-a16d-218bd8dd962a'
                       }}
                />
                <Text>
                    {rating}
                </Text>
            </View>

        </View>
        {socials && <View style={styles.socials}>
            <Image style={styles.icon}
                   source={{
                       uri: 'https://firebasestorage.googleapis.com/v0/b/unify-v3-copy.appspot.com/o/w8oh9snbo4q-216%3A165?alt=media&token=89dbf14c-d466-4886-a37c-1f9e2f2ddbc5'
                   }}/>
            <Image style={styles.icon}
                   source={{
                       uri: 'https://firebasestorage.googleapis.com/v0/b/unify-v3-copy.appspot.com/o/w8oh9snbo4q-216%3A171?alt=media&token=a4f18f39-25b6-4ab9-a367-ffe9e70e5add'
                   }}/>
        </View>}
    </TouchableOpacity>
}