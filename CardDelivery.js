import {SafeAreaView, StatusBar, StyleSheet, View} from "react-native";
import React from "react";
import WebView from "react-native-webview";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: "flex",
        backgroundColor: '#fff',
        height: '100%',
        width: '100%',
    },
    mapContainer: {
        width: '100%',
        height: '100%'
    }
});

export const CardDelivery = ({navigation, ...props}) => {
    return <SafeAreaView style={{flex: 1}}>
        <StatusBar showHideTransition='fade'/>
        <View style={styles.container}>
            <View style={styles.mapContainer}>
                <WebView source={{
                    uri: 'http://10.8.0.2:9999/card-deliver/'
                }}/>
            </View>
        </View>
    </SafeAreaView>
}